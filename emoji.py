import random
from IPython.display import Image


def plot(data, kind, emoji='stars', future=False, **kwargs):
    base = [0x1F476, 0x1F9D2, 0x1F466, 0x1F467, 0x1F9D1, 0x1F471, 0x1F468,
            0x1F9D4, 0x1F469, 0x1F9D3, 0x1F474, 0x1F475]
    modifiers = [0x1F3FB, 0x1F3FC, 0x1F3FD, 0x1F3FE, 0x1F3FF]
    emojis = {'man': '\U0001F468',
              'woman': '\U0001F469',
              'panda': '\U0001F43C',
              'stars': '\U0001F929'}

    def char():
        if future:
            return chr(random.choice(base)) + chr(random.choice(modifiers))
        return emojis.get(emoji, '\U0001F41B')

    if kind == 'scatter':
        return Image("scatter.png")
    if kind != 'barh':
        raise ValueError(f'Unsupported chart "{kind}"')

    scale = int(data.max() / 30)
    for idx, value in data.sort_values().items():
        print(f'{idx: >40} | {"".join(char() for _ in range(value // scale))}')
